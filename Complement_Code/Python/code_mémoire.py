import mysql.connector

# Connection à mysql:---------------------------------------------------------------
# Exemple connection 1
database_origine="classicmodels_V1"
database_migration="classicmodels_V2"

mydb = mysql.connector.connect(host="localhost", 
                               user="antony", 
                               password="choupette")

mycursor = mydb.cursor(dictionary=False)

# Code exemple:---------------------------------------------------------------------
last_id = "SELECT LAST_INSERT_ID();"

def select_bdd(BDD: str, table: str, column: str, value: list, cond_column: str):
    sql = f"SELECT {BDD}.{column} FROM {table} WHERE {cond_column} = %s;"
    mycursor.execute(sql, value)
    return mycursor.fetchall()


def insert_bdd(conn, BDD: str, table: str, column: str, value: list):
    sql = f"INSERT INTO {BDD}.{table} ({column}) VALUES (%s);"
    mycursor.execute(sql, value)
    conn.commit()


# Exemple test de code:
data = [
    {"name":"Tom", "gender":"male"},
    {"name":"Jack", "gender":"male"},
    {"name":"Lee", "gender":"male"}
]
# Récupération nom de colonne sous forme str
columns_many = many_column_str(data)
print(columns_many)
# Insertion valeur dans requète SQL
mycursor.executemany(f"""INSERT INTO {table} ({columns_many}) 
                         VALUES (%(name)s, %(gender)s)""", data)
mydb.commit() 



# list of dict: recup clé ou val
for dico in data:
    for cle in dico.keys():
        columns_many.append(cle)


# ------------------------------------------
# if list_table_bdd == "productlines":
#     # Pas de modification
#     print("Pas de modification")
# elif list_table_bdd == "offices":
#     # Modification à faire ==> code en 3 lettres
#     # Table: offices / Columns: country 
#     if list_dico[0]["country"] == "USA":
#         pass
#         # Code en 3 lettres existant:
#             # USA on conserve
#         # Code en 2 lettres existant:
#             # UK non trouvable car GB ou GBR
#         # Nom en entier existant:
#             # France on change par FRA
#             # Japan on change par JPN
#             # Australia on change par AUS
#     print("Modification à faire")
# elif list_table_bdd == "employees":
#     # Pas de modification
#     print("Pas de modification")
# elif list_table_bdd == "customers":
#     # Modification à faire 
#         # ==> code en 3 lettres
#         # ==> à saisir
#     # Table: customers / Columns: country 
#         # Code en 3 lettres existant:
#             # USA on conserve
#         # Code en 2 lettres existant:
#             # UK non trouvable car GB ou GBR
#         # Nom en entier existant:
#             # Norway on change par NOR - NO
#             # Russia on change par Russian Federation - RUS - RU
#             # Le reste on change par le code à 3 lettres
#     # Table: offices / Columns: language
#     print("Modification à faire")
# elif list_table_bdd == "payments":
#     # Pas de modification
#     print("Pas de modification")
# elif list_table_bdd == "orders":
#     # Modification à faire 
#         # => enum à faire
#     # Table: orders / Columns: status 
#         # Tous les lignes ont un status ==> Rien à faire
#     print("Modification à faire")
# elif list_table_bdd == "products":
#     # Pas de modification
#     print("Pas de modification")
# elif list_table_bdd == "orderdetails":
#     # Pas de modification
#     print("Pas de modification")


# sql = """SELECT DISTINCT Name FROM world.country 
#          WHERE `Name` IN (
#             SELECT DISTINCT country 
#             FROM classicmodels_V1.customers);"""
# mycursor.execute(sql)
# dico_Name_country = mycursor.fetchall()
#     # Liste des pays de la table offices sans doublon
# print(dico_Name_country, "nom pays")
# print()


# print(list_dico) # Affiche la liste de dico
# print(list_dico[0]) # Affiche le dico à l'index 0
# print(list_dico[0]["country"]) # Affiche la colonne du dico: index 0


        # if list_dico[index]["country"] == "USA":
        #     # Code en 3 lettres existant: ==> USA on conserve
        #     print(list_dico[index]["country"], "== USA")

        # if list_dico[index]["country"] == "USA":
        #     # Code en 3 lettres existant: ==> USA on conserve
        #     print(list_dico[index]["country"], "== USA")

# France on change par FRA / Japan on change par JPN / Australia on change par AUS
