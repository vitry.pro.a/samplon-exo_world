CREATE TABLE `productlines` (
  `productLine` varchar(50) PRIMARY KEY,
  `textDescription` varchar(4000) DEFAULT NULL,
  `htmlDescription` mediumtext,
  `image` mediumblob
);

CREATE TABLE `products` (
  `productCode` varchar(15) PRIMARY KEY,
  `productName` varchar(70) NOT NULL,
  `productLine` varchar(50) NOT NULL,
  `productScale` varchar(10) NOT NULL,
  `productVendor` varchar(50) NOT NULL,
  `productDescription` text NOT NULL,
  `quantityInStock` smallint(6) NOT NULL,
  `buyPrice` decimal(10,2) NOT NULL,
  `MSRP` decimal(10,2) NOT NULL
);

CREATE TABLE `offices` (
  `officeCode` varchar(10) PRIMARY KEY,
  `city` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `addressLine1` varchar(50) NOT NULL,
  `addressLine2` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(3) NOT NULL DEFAULT "",
  `postalCode` varchar(15) NOT NULL,
  `territory` varchar(10) NOT NULL
);

CREATE TABLE `employees` (
  `employeeNumber` int PRIMARY KEY,
  `lastName` varchar(50) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `extension` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `officeCode` varchar(10) NOT NULL,
  `reportsTo` int DEFAULT NULL,
  `jobTitle` varchar(50) NOT NULL
);

CREATE TABLE `customers` (
  `customerNumber` int PRIMARY KEY,
  `customerName` varchar(50) NOT NULL,
  `contactLastName` varchar(50) NOT NULL,
  `contactFirstName` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `addressLine1` varchar(50) NOT NULL,
  `addressLine2` varchar(50) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) DEFAULT NULL,
  `postalCode` varchar(15) DEFAULT NULL,
  `country` varchar(3) NOT NULL DEFAULT "",
  `language` varchar(2) NOT NULL DEFAULT "",
  `salesRepEmployeeNumber` int DEFAULT NULL,
  `creditLimit` decimal(10,2) DEFAULT NULL
);

CREATE TABLE `payments` (
  `customerNumber` int,
  `checkNumber` varchar(50) NOT NULL,
  `paymentDate` date NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  PRIMARY KEY (`customerNumber`, `checkNumber`)
);

CREATE TABLE `orders` (
  `orderNumber` int PRIMARY KEY,
  `orderDate` date NOT NULL,
  `requiredDate` date NOT NULL,
  `shippedDate` date DEFAULT NULL,
  `status` ENUM ('Shipped', 'Resolved', 'Cancelled', 'On Hold', 'Disputed', 'In Process') NOT NULL DEFAULT "In Process",
  `comments` text,
  `customerNumber` int NOT NULL
);

CREATE TABLE `orderdetails` (
  `orderNumber` int,
  `productCode` varchar(15) NOT NULL,
  `quantityOrdered` int NOT NULL,
  `priceEach` decimal(10,2) NOT NULL,
  `orderLineNumber` smallint(6) NOT NULL,
  PRIMARY KEY (`orderNumber`, `productCode`)
);

ALTER TABLE `products` ADD FOREIGN KEY (`productLine`) REFERENCES `productlines` (`productLine`);

ALTER TABLE `employees` ADD FOREIGN KEY (`reportsTo`) REFERENCES `employees` (`employeeNumber`);

ALTER TABLE `employees` ADD FOREIGN KEY (`officeCode`) REFERENCES `offices` (`officeCode`);

ALTER TABLE `customers` ADD FOREIGN KEY (`salesRepEmployeeNumber`) REFERENCES `employees` (`employeeNumber`);

ALTER TABLE `payments` ADD FOREIGN KEY (`customerNumber`) REFERENCES `customers` (`customerNumber`);

ALTER TABLE `orders` ADD FOREIGN KEY (`customerNumber`) REFERENCES `customers` (`customerNumber`);

ALTER TABLE `orderdetails` ADD FOREIGN KEY (`orderNumber`) REFERENCES `orders` (`orderNumber`);

ALTER TABLE `orderdetails` ADD FOREIGN KEY (`productCode`) REFERENCES `products` (`productCode`);
