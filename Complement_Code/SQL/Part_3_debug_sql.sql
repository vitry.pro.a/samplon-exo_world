USE world;
SELECT DISTINCT status FROM orders;


USE world; -- Table: country / Columns: Code et Code2

SELECT * FROM country;

SELECT * FROM country WHERE `Code` = "USA";
-- Test de champs en 2 lettres
SELECT * FROM country WHERE `Code2` = "UK";
-- Test de champs avec Nom
SELECT * FROM country WHERE `Name` = "Russian Federation"; -- "United Kingdom" ou "Royaume-Uni"


USE classicmodels_V1; -- Table: offices / Columns: country

SELECT * FROM offices;
SELECT DISTINCT country FROM offices;
    -- Test de champs en 3 lettres
SELECT * FROM offices WHERE `country` = "USA";
-- Test de champs en 2 lettres
SELECT * FROM offices WHERE `country` = "UK";
-- Test de champs avec Nom
SELECT * FROM offices WHERE `country` = "Japan";


USE classicmodels_V1; -- Table: customers / Columns: country

SELECT DISTINCT country FROM customers;
-- Test de champs en 3 lettres
SELECT * FROM customers WHERE `country` = "USA";
-- Test de champs en 2 lettres
SELECT * FROM customers WHERE `country` = "UK";
-- Test de champs avec Nom
SELECT * FROM customers WHERE `country` = "Japan";

SELECT * FROM customers WHERE `customerNumber` = "114";

-- Test global
SELECT * FROM customers;
SELECT DISTINCT country FROM classicmodels_V1.customers;
SELECT DISTINCT Name FROM world.country 
WHERE `Name` IN (SELECT DISTINCT country FROM classicmodels_V1.customers);



USE classicmodels_V1; -- Table: orders / Columns: status 

SELECT DISTINCT status FROM orders;
-- Shipped / Resolved / Cancelled / On Hold / Disputed / In Process

SELECT COUNT(status) FROM orders;
SELECT COUNT(status) FROM orders WHERE `status` = "In Process";



USE classicmodels_V1; -- Table: customers / Columns: language 

-- 9. Liste des pays avec langue officielle, le code3 et le status officielle
SELECT country.`Code`, country.`Name`, countrylanguage.`Language`, countrylanguage.`IsOfficial`
FROM country
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
WHERE countrylanguage.`IsOfficial` = "T";
SELECT * FROM country
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`;

-- Requête final pour correspondance code/code2/name/langue_off
SELECT country.`Code`, country.`Code2`, country.`Name`, countrylanguage.`Language`
FROM country
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
WHERE countrylanguage.`IsOfficial` = "T";


USE classicmodels_V2; -- Vérification migration

SELECT * FROM customers WHERE `country` = "None";
SELECT * FROM customers WHERE `language` = "None";
SELECT * FROM offices WHERE `country` = "None";