USE world;
-- 1. Liste des type de gouvernement avec nombre de pays pour chaque.
SELECT `GovernmentForm`, COUNT(`GovernmentForm`) FROM country GROUP BY `GovernmentForm`;

-- 3. D’apres la BDD, combien de personne dans le monde parle anglais ?

-- I - Code country qui parle anglais:
SELECT * FROM countrylanguage WHERE `Language`= "English";
-- II - Pays qui parle anglais
SELECT * FROM country
WHERE `Code` IN (
    SELECT `CountryCode` FROM countrylanguage WHERE `Language`= "English");
-- III - Ville qui parle anglais
SELECT * FROM city WHERE `CountryCode` IN (
    SELECT `CountryCode` FROM countrylanguage WHERE `Language`= "English");

-- IV - Pays + Ville qui parle anglais
SELECT country.`Code`, countrylanguage.`Language`, countrylanguage.`Percentage`, 
country.`Name`, country.`Population`, 
CAST((country.`Population` * countrylanguage.`Percentage`) / 100 AS DECIMAL(10, 1)) AS country_speak_english, 
country.`Capital`, city.`Name`, city.`Population`,
CAST((city.`Population` * countrylanguage.`Percentage`) / 100 AS DECIMAL(10, 1)) AS city_speak_english
FROM country
JOIN city ON country.`Capital` = city.`ID`
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
WHERE `Language`= "English";

    -- Nombre de personne qui parle anglais dans le monde
SELECT SUM(CAST((country.`Population` * countrylanguage.`Percentage`) / 100 AS DECIMAL(10, 1))) 
AS Somme_speak_english
FROM country
JOIN city ON country.`Capital` = city.`ID`
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
WHERE `Language`= "English";

    -- Nombre de personne dans le monde
SELECT SUM(country.`Population`) AS Somme_population
FROM country
JOIN city ON country.`Capital` = city.`ID`
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
WHERE `Language`= "English";

    -- Nombre de personne qui parle anglais dans le monde en %
SELECT (
    SELECT SUM(CAST((country.`Population` * countrylanguage.`Percentage`) / 100 AS DECIMAL(10, 1))) 
    AS Somme_speak_english
    FROM country
    JOIN city ON country.`Capital` = city.`ID`
    JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
    WHERE `Language`= "English"
) / (
    SELECT SUM(country.`Population`) AS Somme_population
    FROM country
    JOIN city ON country.`Capital` = city.`ID`
    JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
    WHERE `Language`= "English"
) * 100 AS Pourcentage_speak_english;

-- 4. Faire la liste des langues avec le nombre de locuteur, de la plus 
-- parlée à la moins parlée.
SELECT SUM(CAST((country.`Population` * countrylanguage.`Percentage`) / 100 AS DECIMAL(10, 1))) 
AS Somme_speaker,
countrylanguage.`Language`
FROM country
JOIN city ON country.`Capital` = city.`ID`
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
GROUP BY countrylanguage.`Language`
ORDER BY Somme_speaker DESC;

-- 6. Faire la liste des pays qui ont plus 10 000 000 d’hab. avec leur 
-- capitale et le % de la population qui habite dans la capitale.
    -- Liste des pays qui ont plus de 10M hab 
SELECT * FROM country
JOIN city ON country.`Capital` = city.`ID`
WHERE country.`Population` >= 10000000;

    -- Capitale avec le % d'hab -> (hab ville / hab pays)
SELECT country.`Code`, country.`Name`, country.`Population`,
country.`Capital`, city.`Name`, city.`Population`,
CAST(city.`Population`/ country.`Population`AS DECIMAL(3, 1)) AS Pourcentage_hab_capital
FROM country
JOIN city ON country.`Capital` = city.`ID`;

    -- Pays + capital
SELECT country.`Code`, country.`Name`, country.`Population`,
country.`Capital`, city.`Name`, city.`Population`,
CAST(city.`Population`/ country.`Population`AS DECIMAL(3, 1)) AS Pourcentage_hab_capital
FROM country
JOIN city ON country.`Capital` = city.`ID`
WHERE country.`Population` >= 10000000
ORDER BY country.`Population` DESC;

-- 7. Liste des 10 pays avec le plus fort taux de croissance entre n et 
-- n-1 avec le % de croissance
SELECT `Code`, `Name`, `GNP`, `GNPOld`, 
CAST((`GNP` / `GNPOld`)*100 AS DECIMAL(16,1)) AS Pourcentage_croissance
FROM country
WHERE `GNPOld` >= 0
ORDER BY Pourcentage_croissance DESC
LIMIT 10;

-- 8. Liste des pays plurilingues avec pour chacun le nombre de langues parlées.
SELECT country.`Name`, COUNT(countrylanguage.`Language`) AS nombre_langue_parle
FROM country
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
GROUP BY country.`Name`
ORDER BY nombre_langue_parle DESC;

-- 9. Liste des pays avec plusieurs langues officielles, le nombre de langues 
-- officielle et le nombre de langues du pays.
SELECT country.`Name`, COUNT(countrylanguage.`Language`) AS nb_langue_officiel_parle, 
countrylanguage.`IsOfficial`, COUNT(countrylanguage.`Language`) AS nombre_langue_parle
FROM country
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
WHERE countrylanguage.`IsOfficial` = "T"
GROUP BY country.`Name`
ORDER BY nombre_langue_parle DESC;

-- 10. Liste des langues parlées en France avec le % pour chacune.
SELECT country.`Name`, countrylanguage.`Language`, countrylanguage.`Percentage`
FROM country
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
WHERE country.`Name` = "France";

-- 11. Pareil en chine.
SELECT country.`Name`, countrylanguage.`Language`, countrylanguage.`Percentage`
FROM country
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
WHERE country.`Name` = "China";

-- 12. Pareil aux états unis.
SELECT country.`Name`, countrylanguage.`Language`, countrylanguage.`Percentage`
FROM country
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
WHERE country.`Name` = "United States";

-- 13. Pareil aux UK.
SELECT country.`Name`, countrylanguage.`Language`, countrylanguage.`Percentage`
FROM country
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
WHERE country.`Name` = "United Kingdom";

-- 14. Pour chaque région quelle est la langue la plus parler 
-- et quel pourcentage de la population la parle.
SELECT country.`Region`, countrylanguage.`Language`, countrylanguage.`Percentage`
FROM country
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`;

-- 15. Est-ce que la somme des pourcentages de langues parlées dans un pays 
-- est égale à 100 ? Pourquoi ?
SELECT country.`Name`, SUM(countrylanguage.`Percentage`) AS pourcentage_sum
FROM country
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
GROUP BY country.`Name`;

SELECT country.`Name`, countrylanguage.`Language`, countrylanguage.`Percentage`
FROM country
JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`;