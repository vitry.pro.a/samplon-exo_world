USE classicmodels_V2;
-- Questions.
-- 1. Pour chaque pays (avec au moins 1 client) le nombre de client, 
-- la langue officielle du pays (si plusieurs, peu importe laquelle), le CA total.
SELECT country, COUNT(customerNumber), SUM(creditLimit), language FROM customers
GROUP BY country, language;

-- 2. La liste des 10 clients qui ont rapporté le plus gros CA, avec le CA correspondant.
SELECT customerNumber, MAX(creditLimit) FROM customers
GROUP BY customerNumber
ORDER BY creditLimit DESC
LIMIT 10;

-- 3. La durée moyenne (en jours) entre les dates de commandes et 
-- les dates d’expédition (de la même commande).
SELECT `orderDate`, `shippedDate`, DATEDIFF(`shippedDate`, `orderDate`) 
FROM orders;
SELECT AVG(DATEDIFF(`shippedDate`, `orderDate`)) AS Durée_moyenne
FROM orders;

-- 4. Les 10 produits les plus vendus.
SELECT productCode, SUM(quantityOrdered) AS Somme 
FROM orderdetails
GROUP BY productCode
ORDER BY Somme DESC
LIMIT 10;

-- 5. Pour chaque pays le produits le plus vendu.
SELECT customers.`country`, orderdetails.`productCode`, SUM(orderdetails.`quantityOrdered`) AS somme
FROM orderdetails
JOIN orders ON orderdetails.`orderNumber` = orders.`orderNumber`
JOIN customers ON orders.`customerNumber` = customers.`customerNumber`
GROUP BY customers.`country`,  orderdetails.`productCode`
ORDER BY somme DESC;

-- 6. Le produit qui a rapporté le plus de bénéfice.
SELECT productCode, SUM(priceEach) AS Somme 
FROM orderdetails
GROUP BY productCode
ORDER BY Somme DESC
LIMIT 1;

-- 7. La moyenne des différences entre le MSRP et le prix de vente (en pourcentage).


-- 8. Pour chaque pays (avec au moins 1 client) le nombre de bureaux dans le même pays.

