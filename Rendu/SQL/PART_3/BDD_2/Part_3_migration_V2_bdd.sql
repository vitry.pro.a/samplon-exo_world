USE classicmodels_V2;

-- 1. Utiliser classicmodel.customer.country pour stocker la clef 
-- du pays venant de la table world.country
ALTER TABLE customers
MODIFY country varchar(3) NOT NULL;

-- 2. Idem pour classicmodel.offices.country.
ALTER TABLE offices
MODIFY country varchar(3) NOT NULL DEFAULT "";

-- 3. Remplacer le type du champ classicmodel.orders.status par un 
-- enum de tous les états différents dans la base de données.
ALTER TABLE orders
MODIFY status ENUM(
    "Shipped",
    "Resolved",
    "Cancelled",
    "On Hold",
    "Disputed",
    "In Process") NOT NULL DEFAULT "In Process";

-- 4. Ajouter un champ classicmodel.customer.language qui devra 
-- contenir le code de la langue avec laquelle s’adresser au client 
-- en ISO 639-1.
ALTER TABLE customers
ADD language varchar(2) NOT NULL;