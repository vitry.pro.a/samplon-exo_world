## Télécharger et importer : [mysql sample database](https://www.mysqltutorial.org/getting-started-with-mysql/mysql-sample-database/)


# Modification du modèle.
1. Faire une version 2 du modèle de donnée. On veut apporter les changements suivants:
    1. Utiliser classicmodel.customer.country pour stocker la clef du pays venant de la table world.country
    2. Idem pour classicmodel.offices.country.
    3. Remplacer le type du champ classicmodel.orders.status par un enum de tous les états différents dans la base de données.
    4. Ajouter un champ classicmodel.customer.language qui devra contenir le code de la langue avec laquelle s’adresser au client en ISO 639-1.
2. Écrire le code SQL permettant de faire ces modifications dans un fichier migration.sql.

# Migration de la base.
Pour appliquer notre migration on doit:
   1. Créer une nouvelle BDD sur le même modèle que la première (sans les données).
   2. Appliquer la migration (changer le modèle).
   3. Importer les données depuis la première base de données en appliquant les modifications nécessaires (par exemple remplacer le nom du pays par la PK de world.country).

> pour la langue des clients, on mettra la première (la plus courante) langue officielle du pays.

# Questions.
On veut :
1. Pour chaque pays (avec au moins 1 client) le nombre de client, la langue officielle du pays (si plusieurs, peu importe laquelle), le CA total.
+---------+-----------------------+------------------+----------+
| country | COUNT(customerNumber) | SUM(creditLimit) | language |
+---------+-----------------------+------------------+----------+
| FRA     |                    12 |        932300.00 | FR       |
| USA     |                    36 |       2811700.00 | US       |
| AUS     |                     5 |        430300.00 | AU       |
| NOR     |                     3 |        273600.00 | NO       |
| POL     |                     1 |             0.00 | PL       |
| DEU     |                    13 |        257100.00 | DE       |
| ESP     |                     7 |        517800.00 | ES       |
| SWE     |                     2 |        169500.00 | SE       |
| DNK     |                     2 |        204200.00 | DK       |
| SGP     |                     3 |        201700.00 | SG       |
| PRT     |                     2 |             0.00 | PT       |
| JPN     |                     2 |        175600.00 | JP       |
| FIN     |                     3 |        285800.00 | FI       |
| GBR     |                     5 |        443700.00 | GB       |
| IRL     |                     2 |         69400.00 | IE       |
| CAN     |                     3 |        228600.00 | CA       |
| HKG     |                     1 |         58600.00 | HK       |
| ITA     |                     4 |        388800.00 | IT       |
| CHE     |                     3 |        141300.00 | CH       |
| NLD     |                     1 |             0.00 | NL       |
| BEL     |                     2 |        103400.00 | BE       |
| NZL     |                     4 |        362500.00 | NZ       |
| ZAF     |                     1 |             0.00 | ZA       |
| AUT     |                     2 |        117000.00 | AT       |
| PHL     |                     1 |         81500.00 | PH       |
| RUS     |                     1 |             0.00 | RU       |
| ISR     |                     1 |             0.00 | IL       |
+---------+-----------------------+------------------+----------+

2. La liste des 10 clients qui ont rapporté le plus gros CA, avec le CA correspondant.
+----------------+------------------+
| customerNumber | MAX(creditLimit) |
+----------------+------------------+
|            141 |        227600.00 |
|            124 |        210500.00 |
|            298 |        141300.00 |
|            151 |        138500.00 |
|            187 |        136800.00 |
|            146 |        123900.00 |
|            286 |        123700.00 |
|            386 |        121400.00 |
|            227 |        120800.00 |
|            259 |        120400.00 |
+----------------+------------------+

3. La durée moyenne (en jours) entre les dates de commandes et les dates d’expédition (de la même commande).
+----------------+
| Durée_moyenne  |
+----------------+
|         3.7564 |
+----------------+

4. Les 10 produits les plus vendus.
+-------------+-------+
| productCode | Somme |
+-------------+-------+
| S18_3232    |  1808 |
| S18_1342    |  1111 |
| S700_4002   |  1085 |
| S18_3856    |  1076 |
| S50_1341    |  1074 |
| S18_4600    |  1061 |
| S10_1678    |  1057 |
| S12_4473    |  1056 |
| S18_2319    |  1053 |
| S24_3856    |  1052 |
+-------------+-------+

5. Pour chaque pays le produits le plus vendu.
Mauvaise requête

6. Le produit qui a rapporté le plus de bénéfice.
+-------------+---------+
| productCode | Somme   |
+-------------+---------+
| S18_3232    | 8074.17 |
+-------------+---------+

7. La moyenne des différences entre le MSRP et le prix de vente (en pourcentage).
NON-FAIT

8. Pour chaque pays (avec au moins 1 client) le nombre de bureaux dans le même pays.
NON-FAIT