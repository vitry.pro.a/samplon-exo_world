## Faire un fichier markdown avec :
1. le modèle physique des données

Grâce au fichier sql, j'ai pu généré les tables sous dbdiagram.io
![MLD](Schema/World_db.png)

2. pour chaque type utilisé pour au moins un attribut : le definir et expliquer pourquoi on a choisit ce type ?
Pour la table country:
    Attribut      |  Type                   |   Explication
    --------------|-------------------------|------------------------------------------------------------
    "Code"        |  char(3)                |   L'information est une chaine de caractère donc char
    "Continent"   |  country_Continent_enum |   L'information est une chaine de caractère prédéterminé  par la sélection donc enum
    "SurfaceArea" |  decimal(10,2)          |   L'information est nombre à virgule donc decimal / float
    "IndepYear"   |  smallint               |   L'information est nombre de petite taille donc smallint
    "Population"  |  int                    |   L'information est nombre donc int

3. une "explication" ou "description" des informations que l'on peut tirer de chaque colonne de chaque table.
### Table city:
  - "ID": nombre identifiant la ville dans la BDD (clé primaire)
  - "Name": nom de la ville (Capital du pays)
  - "CountryCode": code identifiant du pays où se situe la ville - Norme: List of ISO 3166 country codes
  - "District": il s'agit du quartier où se trouve la ville - Incorrect
  - "Population": nombre de personne qui habitent dans la ville
### Table country:
  - "Code": code identifiant du pays
  - "Name": nom du pays
  - "Continent": nom du continent où se trouve le pays
  - "Region": nom de la region où se trouve le pays
  - "SurfaceArea": surface du pays - Unités: en KM²
  - "IndepYear": année de prise d'indépendance
  - "Population": nombre de personne qui habitent dans le pays
  - "LifeExpectancy": moyenne de l'espérance de vie du pays
  - "GNP": PNB Produit National brut
  - "GNPOld": PIB Produit Intérieur brut
  - "LocalName": nom donné par les habitants du pays
  - "GovernmentForm": Type de gouvernement
  - "HeadOfState": Représentant du pays
  - "Capital": clé étrangère "ID" de la ville capital du pays
  - "Code2": Abréviation du nom du pays - Norme: List of ISO 3166 country codes
### Table countrylanguage:
  - "CountryCode": code identifiant - clé étrangère "Code" du pays
  - "Language": Langue parler dans le pays
  - "IsOfficial": Identification si la langue est la langue officiel du pays
  - "Percentage": pourcentage de la population qui parle la langue dans le pays

4. une liste, pour chaque table, des contraintes (pourquoi il y a ces contraintes ?)
### Table city:
> L'attribut "CountryCode" est contraint de tel sorte que seul l'id de la table country est saisissable.
Il s'agit d'une clé étrangère de la table country.

  Atribut       |   Setting                   | Commentaire
----------------|-----------------------------|---------------------------------------------------------
  "ID"          |   [pk, not null, increment] | Clé primaire, qui est non null et qui s'auto-incrémente
  "Name"        |   [not null, default: ""]   | qui est non null et par défaut une chaine vide
  "CountryCode" |   [not null, default: ""]   | qui est non null et par défaut une chaine vide
  "District"    |   [not null, default: ""]   | qui est non null et par défaut une chaine vide
  "Population"  |   [not null, default: "0"]  | qui est non null et par défaut 0

### Table country:
L'attribut "Code" est une clé primaire qui permet d'identifier chaque pays de manière unique.

  Atribut           |   Setting                      | Commentaire
--------------------|--------------------------------|-------------------------------------------------
  "Code"            |  [pk, not null, default: ""]   | Clé primaire, qui est non null, par défaut une chaine vide
  "Name"            |  [not null, default: ""]       | qui est non null, par défaut une chaine vide
  "Continent"       |  [not null, default: "Asia"]   | qui est non null, par défaut à Asia
  "Region"          |  [not null, default: ""]       | qui est non null, par défaut une chaine vide
  "SurfaceArea"     |  [not null, default: "0.00"]   | qui est non null, par défaut 0,00
  "IndepYear"       |  [default: NULL]               | qui est non null
  "Population"      |  [not null, default: "0"]      | qui est non null, par défaut à 0
  "LifeExpectancy"  |  [default: NULL]               | qui est non null
  "GNP"             |  [default: NULL]               | qui est non null
  "GNPOld"          |  [default: NULL]               | qui est non null
  "LocalName"       |  [not null, default: ""]       | qui est non null, par défaut une chaine vide
  "GovernmentForm"  |  [not null, default: ""]       | qui est non null, par défaut une chaine vide
  "HeadOfState"     |  [default: NULL]               | qui est non null
  "Capital"         |  [default: NULL]               | qui est non null
  "Code2"           |  [not null, default: ""]       | qui est non null, par défaut une chaine vide

### Table countrylanguage:
L'attribut "CountryCode" est contraint de tel sorte que seul l'id de la table country est saisissable.
Il s'agit d'une clé étrangère de la table country.

  Atribut            |   Setting                     | Commentaire
---------------------|-------------------------------|--------------------------------------
  "CountryCode"      |  [not null, default: ""]      | non null, par défaut une chaine vide
  "Language"         |  [not null, default: ""]      | non null, par défaut une chaine vide
  "IsOfficial"       |  [not null, default: "F"]     | non null, par défaut F
  "Percentage"       |  [not null, default: "0.0"]   | non null, par défaut 0,00







