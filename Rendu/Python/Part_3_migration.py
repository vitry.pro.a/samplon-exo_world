import mysql.connector
from utiles import *


# Connection SQL: ------------------------------------------------------------------
list_of_bdd = ["classicmodels_V1", "classicmodels_V2", "world"]

db_dict = connexion_bdd(mysql.connector, list_of_bdd)

# ---------------------------------------------------------------------------------+
# Méthodologie du transfert de base de donnée:                                     |
# ---------------------------------------------------------------------------------+
cond_dictionary=True
# dictionary = False, récup en liste pour refaire en dict via fonction
# dictionary = True, ne retourne pas dans l'ordre

    # -----------------------------------------------------+
    # Etape 1: Nom de table dans l'ordre de migration
    # -----------------------------------------------------+
list_table_bdd = ["productlines", "offices", "employees", "customers", 
                  "payments", "orders", "products", "orderdetails"]

# ICI GESTION POUR FAIRE CHAQUE TABLE (Boucle FOR)
for ind, val in enumerate(list_table_bdd):
    # -------------------------------------------------------+
    # Etape 2: Initialisation BDD d'origine: classicmodels_V1
    # -------------------------------------------------------+
    mydb = db_dict['classicmodels_V1']
    mydb.reconnect()
    mycursor = mydb.cursor(dictionary=cond_dictionary)

    # -----------------------------------------------------+
    # Etape 3: Récupération des lignes de la BDD d'origine
    # -----------------------------------------------------+
    list_dico = select_bdd(mycursor, list_table_bdd[ind], "*")
    mydb.commit()
    mydb.close()

    # -----------------------------------------------------+
    # Etape 4: Modification de la ligne pour BDD migration
    # -----------------------------------------------------+
    """Objectif: Faire de dico de conversion:
            Pour avoir la liste des langues officiel en fonction pays
            Pour avoir la correspondance Name ==> Code3 et Code2 ==> Code3
    """
    # Database: World
    mydb = db_dict['world']
    mydb.reconnect()
    mycursor = mydb.cursor(dictionary=cond_dictionary)

    sql = """SELECT country.`Code`, country.`Code2`, country.`Name`, countrylanguage.`Language` 
            FROM country
            JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
            WHERE countrylanguage.`IsOfficial` = "T";"""
    mycursor.execute(sql)
    # Liste des langues officielles importés
    dico_lang_off_by_country = mycursor.fetchall()

    mydb.commit()
    mydb.close()

    # Datbase: classicmodels_V1
    mydb = db_dict['classicmodels_V1']
    mydb.reconnect()
    mycursor = mydb.cursor(dictionary=cond_dictionary)

        # Pour avoir la liste des pays de la table offices sans doublon (Table: offices)
    exemple_sql = "SELECT DISTINCT country FROM offices;"
    dico_country_offices = select_bdd(mycursor, "offices", "DISTINCT country")
    # Formatage data
    for dico_office in dico_country_offices:
        if dico_office["country"] == "USA":
            dico_country_offices.remove(dico_office)
        elif dico_office["country"] == "UK":
            dico_country_offices.remove(dico_office)

        # Liste des pays de la table curtomers sans doublon (Table: customers)
    exemple_sql = """SELECT DISTINCT country FROM customers;"""
    dico_country_customers = select_bdd(mycursor, "customers", "DISTINCT country")
    # Formatage data
    for dico_customer in dico_country_customers:
        if dico_customer["country"] == "USA":
            dico_country_customers.remove(dico_customer)
        elif dico_customer["country"] == "UK":
            dico_country_customers.remove(dico_customer)
        elif dico_customer["country"].strip() == "Norway":
            dico_country_customers.remove(dico_customer)
        elif dico_customer["country"] == "Russia":
            dico_country_customers.remove(dico_customer)

    mydb.commit()
    mydb.close()

    for index, testing in enumerate(list_dico):
        if list_table_bdd[ind] == "offices":
            # Table: offices / Columns: country 
            if list_dico[index]["country"] == "UK":
                # Code en 2 lettres existant: ==> UK non trouvable car GB ou GBR
                list_dico[index]["country"] = "GBR"
            for lang_country in dico_lang_off_by_country:
                if lang_country["Name"] == list_dico[index]["country"]:
                    # Nom en entier existant: ==> on change par 3 lettres
                    list_dico[index]["country"] = lang_country["Code"]
        elif list_table_bdd[ind] == "customers":
            if "language" not in list_dico[index]:
                list_dico[index]["language"] = None
            # Table: customers / Columns: country 
            if list_dico[index]["country"] == "UK":
                # Code en 2 lettres existant: ==> UK non trouvable car GB ou GBR
                list_dico[index]["country"] = "GBR"
            elif list_dico[index]["country"].strip() == "Norway":
                # Norway on change par NOR - NO
                list_dico[index]["country"] = "NOR"
            elif list_dico[index]["country"] == "Russia":
                # Russia on change par Russian Federation - RUS - RU
                list_dico[index]["country"] = "RUS"
            for lang_country in dico_lang_off_by_country:
                if list_dico[index]["country"] == lang_country["Name"]:
                    # Nom en entier existant: ==> le reste on change par le code à 3 lettres
                    list_dico[index]["country"] = lang_country["Code"]
                if list_dico[index]["country"] == lang_country["Code"] and list_dico[index]["language"] == None:
                    # Table: offices / Columns: language
                    list_dico[index]["language"] = lang_country["Code2"]

    # --------------------------------------------------------+
    # Etape 6: Initialisation BDD migration: classicmodels_V2
    # --------------------------------------------------------+
    mydb = db_dict['classicmodels_V2']
    mydb.reconnect()
    mycursor = mydb.cursor(dictionary=cond_dictionary)

    # -----------------------------------------------------+
    # Etape 7: Insertion d'une ligne dans la BDD migration
    # -----------------------------------------------------+

    # Formatage data
    columns_many_table = many_column_str(list_dico)
    columns_many_value = format_many_column_str(list_dico)
    mycursor.executemany(f"""INSERT INTO {list_table_bdd[ind]} ({columns_many_table}) 
                             VALUES ({columns_many_value})""", list_dico)
    mydb.commit()
    print("Table en cours:", list_table_bdd[ind])
    print("Nombre de lignes insérées :", mycursor.rowcount)
    mydb.close()