# Fonctions d'appel BDD via SQL:----------------------------------------------------
def connexion_bdd(connector, list_of_bdd: list):
    """Créer un dictionnaire avec les différentes connections
    Args:
        connector (_type_): connector
        list_of_bdd (list): liste des noms des databases à connecté
    Returns:
        _type_: dictionnaire avec les différentes connections
    """
    db_dict = {}
    for NAME in list_of_bdd:
        db_dict[NAME] = connector.connect(host="localhost", user="antony", 
                                                password="choupette", database=NAME)
    return db_dict


def select_bdd(cursor, table: str, column: str):
    """Interroge la BDD
    Args:
        cursor (_type_): _description_
        table (str): _description_
        column (str): _description_
    Returns:
        _type_: Resultat
    """
    cursor.execute(f"SELECT {column} FROM {table};")
    return cursor.fetchall()


def select_bdd_with_cond(cursor, table: str, column: str, value: list, cond_column: str):
    """Interroge la BDD avec une condition
    Args:
        cursor (_type_): _description_
        table (str): _description_
        column (str): _description_
        value (list): _description_
        cond_column (str): _description_
    Returns:
        _type_: Resultat
    """
    sql = f"SELECT {column} FROM {table} WHERE {cond_column} = %s;"
    cursor.execute(sql, value)
    return cursor.fetchall()


def insert_bdd(conn, cursor, BDD: str, table: str, column: str, value: list):
    """Insert dans la BDD
    Args:
        conn (_type_): _description_
        cursor (_type_): _description_
        BDD (str): _description_
        table (str): _description_
        column (str): _description_
        value (list): _description_
    """
    sql = f"INSERT INTO {BDD}.{table} ({column}) VALUES (%s);"
    cursor.execute(sql, value)
    conn.commit()


def list_of_column_bdd(cursor, BDD: str, table: str) -> list:
    """Interroge la BDD pour avoir la liste des colonnes d'une table
    Args:
        cursor (_type_): _description_
        BDD (str): _description_
        table (str): _description_
    Returns:
        _type_: liste des colonnes
    """
    sql = f"""SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS
              WHERE TABLE_SCHEMA = %s
              AND TABLE_NAME = %s;"""
    cursor.execute(sql, [BDD, table])
    list_column_bdd = cursor.fetchall()
    list_column = []
    for ind, val in enumerate(list_column_bdd):
        list_column.insert(ind, val[0])
    return list_column


def list_to_dict(list_column, list_value):
    """Transforme une liste en dictionnaire
    Args:
        list_column (_type_): _description_
        list_value (_type_): _description_
    Returns:
        _type_: dictionnaire
    """
    # dict_from_list = dict(zip(list_column, list_value))
    dictionnary = {}
    for key, value in zip(list_column,list_value):
        dictionnary[key] = value
    return dictionnary


def many_column_str(list_column):
    # Récupération nom de colonne sous forme str
    # Résultat: customerNumber, customerName, contactLastName, 
    # contactFirstName, phone, addressLine1, addressLine2, city, 
    # state, postalCode, country, salesRepEmployeeNumber, creditLimit
    columns_many_list = []
    for cle in list_column[0].keys():
        columns_many_list.append(cle)
    return ", ".join(columns_many_list)


def format_many_column_str(list_column):
    # Récupération nom de colonne sous forme str
    # Résultat: %(name)s, %(gender)s
    columns_many_list = []
    for cle in list_column[0].keys():
        columns_many_list.append(f"%({cle})s")
    return ", ".join(columns_many_list)