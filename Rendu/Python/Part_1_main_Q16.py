import plotly.graph_objects as go
import pandas as pd
import mysql.connector as connection

mydb = connection.connect(host="localhost", 
                            database = 'world', 
                            user="antony", 
                            passwd="choupette", 
                            use_pure=True)

try:
    query = "SELECT * FROM country;"
    df = pd.read_sql(query, mydb)
    mydb.close() #close the connection
except Exception as e:
    mydb.close()
    print(str(e))

print(df)

fig = go.Figure(data=go.Choropleth(
    locations = df['Code'],
    z = df['GNP'],
    text = df['Name'],
    colorscale = 'Blues',
    autocolorscale=False,
    reversescale=True,
    marker_line_color='darkgray',
    marker_line_width=0.5,
    colorbar_tickprefix = '$',
    colorbar_title = 'GNP',
))

fig.update_layout(
    title_text='BDD Global GNP',
    geo=dict(
        showframe=False,
        showcoastlines=False,
        projection_type='equirectangular'
    ),
    annotations = [dict(
        x=0.55,
        y=0.1,
        xref='paper',
        yref='paper',
        text='Source: BDD',
        showarrow = False
    )]
)

fig.show()